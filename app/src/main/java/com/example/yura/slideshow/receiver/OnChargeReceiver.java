package com.example.yura.slideshow.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.yura.slideshow.MainActivity;

public class OnChargeReceiver extends BroadcastReceiver{
    private static final String TAG = OnChargeReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "chargeBatteryReceiver: " + intent.getAction());
        SharedPreferences sPref = context.getSharedPreferences(MainActivity.MAIN_PREFERECES, context.MODE_PRIVATE);
        boolean startOnPower = sPref.getBoolean(MainActivity.STATE_START_POWER_ON, false);
        if (startOnPower) {
            Intent startIntent = new Intent(context.getApplicationContext(), MainActivity.class);
            startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(startIntent);
        }
        }
    }



