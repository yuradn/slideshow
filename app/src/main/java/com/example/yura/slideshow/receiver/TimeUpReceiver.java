package com.example.yura.slideshow.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.yura.slideshow.MainActivity;

public class TimeUpReceiver extends BroadcastReceiver {
    private static final String TAG=TimeUpReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG,"onReceive");

        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setAction("time");

        context.startActivity(i);

    }
}
