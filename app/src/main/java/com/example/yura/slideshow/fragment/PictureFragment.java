package com.example.yura.slideshow.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.yura.slideshow.MainActivity;
import com.example.yura.slideshow.R;
import com.marvinlabs.widget.slideshow.SlideShowAdapter;
import com.marvinlabs.widget.slideshow.SlideShowView;
import com.marvinlabs.widget.slideshow.adapter.BitmapAdapter;
import com.marvinlabs.widget.slideshow.adapter.RemoteBitmapAdapter;
import com.marvinlabs.widget.slideshow.playlist.SequentialPlayList;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.List;


public class PictureFragment extends Fragment {
    private final static String TAG = PictureFragment.class.getSimpleName();
    SlideShowView slideShowView = null;
    TextView textPicture;
    private boolean pause = false;
    public Handler playStopHandler;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_picture, null);
        textPicture = (TextView) v.findViewById(R.id.textPicture);

        if (((MainActivity)getActivity()).getDirectory().equals("")) {
            textPicture.setText(getResources().getString(R.string.no_directory));
        }

        slideShowView = (SlideShowView) v.findViewById(R.id.slideshow);

        if (!((MainActivity) getActivity()).getDirectory().equals("")) startPlay();

        playStopHandler = new Handler() {

            @Override
            public void handleMessage(Message msg) {

                if (!pause&&((MainActivity) getActivity()).isPlay()) {
                    slideShowView.pause();
                    pause=true;
                } else if (pause&&((MainActivity) getActivity()).isPlay()) {
                    pause=false;
                    slideShowView.resume();
                }
            }
        };

        return v;
    }

    public void starpStopPlay() {
        if (((MainActivity) getActivity()).isPlay()) {
            stopPlay();
        } else {
            startPlay();
        }
    }


    public void setKeyPlayActive(){
        textPicture.setVisibility(View.INVISIBLE);
    }

    public void startPlay(){
        //String path = Environment.getExternalStorageDirectory().toString()+"/Pictures";
        String path = ((MainActivity)getActivity()).getDirectory();
        Log.d("Files", "Path: " + path);
        File f = new File(path);

        FileFilter filter = new FileFilter() {

            private final List<String> exts = Arrays.asList("jpeg", "jpg",
                    "png", "bmp", "gif");

            @Override
            public boolean accept(File pathname) {
                String ext;
                String path = pathname.getPath();
                ext = path.substring(path.lastIndexOf(".") + 1);
                return exts.contains(ext);
            }
        };


        File file[] = f.listFiles(filter);

        Log.d("Files", "Size: "+ file.length);
        String files[] = new String[file.length];
        for (int i=0; i < file.length; i++)
        {
            files[i]=path+"/"+file[i].getName();
            Log.d("Files", "FileName:" + files[i]);

        }

        slideShowView.setOnSlideClickListener(new SlideShowView.OnSlideClickListener() {
            @Override
            public void onItemClick(SlideShowView slideShowView, int i) {
                playStopHandler.sendEmptyMessageDelayed(1,100L);
            }
        });

        slideShowView.setAdapter(createPathAdapter(files));
        SequentialPlayList sequentialPlayList = new SequentialPlayList();
        sequentialPlayList.setSlideDuration(((MainActivity)getActivity()).getInterval()*1000);
        slideShowView.setPlaylist(sequentialPlayList);
        slideShowView.play();
        ((MainActivity)getActivity()).setPlay(true);
    }

    public void stopPlay(){
        slideShowView.stop();
        ((MainActivity)getActivity()).getMenu().removeItem(MainActivity.MENU_STOP_PLAY);
        ((MainActivity)getActivity()).setPlay(false);
    }

    public void changeDuration(){
        SequentialPlayList sequentialPlayList = new SequentialPlayList();
        sequentialPlayList.setSlideDuration(((MainActivity)getActivity()).getInterval()*1000);
        slideShowView.setPlaylist(sequentialPlayList);
    }

    private SlideShowAdapter createRemoteAdapter() {
        String[] slideUrls = new String[]{
                "http://lorempixel.com/1280/720/sports",
                "http://lorempixel.com/1280/720/nature",
                "http://lorempixel.com/1280/720/people",
                "http://lorempixel.com/1280/720/city",
        };
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 2;
        SlideShowAdapter adapter = new RemoteBitmapAdapter(getActivity(), Arrays.asList(slideUrls), opts);
        return adapter;
    }

    private SlideShowAdapter createPathAdapter(final String[] files) {

        SlideShowAdapter adapter = new BitmapAdapter(getActivity()) {
            @Override
            protected void loadBitmap(int i) {
                Bitmap bitmap=null;
                try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                options.inSampleSize = 2;
                bitmap = BitmapFactory.decodeFile(files[i], options);
                Log.d(TAG,""+bitmap.getByteCount());
                } catch (Exception e) {
                    Log.d(TAG,"loadBitmap:"+e.toString());
                }
                super.onBitmapLoaded(i,bitmap);
            }

            @Override
            public int getCount() {
                return files.length;
            }

            @Override
            public Object getItem(int position) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                return BitmapFactory.decodeFile(files[position], options);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            protected void onBitmapLoaded(int position, Bitmap bitmap) {
                super.onBitmapLoaded(position, bitmap);

            }
        };
        return adapter;
    }

}
