package com.example.yura.slideshow.fragment;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.yura.slideshow.MainActivity;
import com.example.yura.slideshow.R;
import com.example.yura.slideshow.receiver.TimeStopReceiver;
import com.example.yura.slideshow.receiver.TimeUpReceiver;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;

import java.util.Calendar;
import java.util.GregorianCalendar;


public class SettingsFragment extends Fragment {
    private final static String TAG = "SettingsFragment";
    public View v;
    private static TextView textInterval, textChooseDirectory, textStartTimeChoose, textStopTimeChoose;
    private CheckBox startIfCharge, autoexec;
    public static String time;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Choose directory
        v = inflater.inflate(R.layout.fragment_settings, null);

        textChooseDirectory = (TextView) v.findViewById(R.id.textChooseDirectory);
        textStartTimeChoose = (TextView) v.findViewById(R.id.textStartTimeChoose);
        textStopTimeChoose  = (TextView) v.findViewById(R.id.textStopTimeChoose);

        startIfCharge = (CheckBox) v.findViewById(R.id.checkPowerOn);
        startIfCharge.setChecked(((MainActivity)getActivity()).isIfChargeStartActivity());
        startIfCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setIfChargeStartActivity(startIfCharge.isChecked());
            }
        });

        autoexec      = (CheckBox) v.findViewById(R.id.checkAutoStart);
        autoexec.setChecked(((MainActivity)getActivity()).isAutoexec());
        autoexec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setAutoexec(autoexec.isChecked());
            }
        });

        v.findViewById(R.id.chooseDirectory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create the ACTION_GET_CONTENT Intent
                Intent chooserIntent = new Intent(getActivity(), DirectoryChooserActivity.class);

                // Optional: Allow users to create a new directory with a fixed name.
                chooserIntent.putExtra(DirectoryChooserActivity.EXTRA_NEW_DIR_NAME,
                        "DirChooserSample");

                // REQUEST_DIRECTORY is a constant integer to identify the request, e.g. 0
                startActivityForResult(chooserIntent, MainActivity.REQUEST_CHOOSER_DIR);

            }
        });

        setTextChooseDirectory();

        //Choose start time
        ((TextView)v.findViewById(R.id.textStartTimeChoose)).setText(((MainActivity)getActivity()).getTimeStart());
        v.findViewById(R.id.buttonSetStartTimeChoose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time="start";
                showTimePickerDialog();
            }
        });

        //Choose stop time
        ((TextView)v.findViewById(R.id.textStopTimeChoose)).setText(((MainActivity)getActivity()).getTimeStop());
        v.findViewById(R.id.buttonSetStopTimeChoose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time="stop";
                showTimePickerDialog();
            }
        });

        textInterval = (TextView) v.findViewById(R.id.textInterval);
        int progress = ((MainActivity)getActivity()).getInterval();
        String str="";
        if (progress<10) str="0"+progress;
        else str=""+progress;
        textInterval.setText(str);

        //Choose interval
        SeekBar seekBar = (SeekBar) v.findViewById(R.id.seekBar);
        seekBar.setProgress(progress);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ((MainActivity)getActivity()).setInterval(progress);
                String str="";
                if (progress<10) str="0"+progress;
                else str=""+progress;
                textInterval.setText(str);
                if (((MainActivity)getActivity()).isPlay()) {
                    ((MainActivity)getActivity()).setChangeDuration(true);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        return v;
    }

    public void setTextChooseDirectory(){
        Log.d(TAG,"Directory: "+((MainActivity)getActivity()).getDirectory());
        textChooseDirectory.setText(((MainActivity)getActivity()).getDirectory());
    }

    public void showTimePickerDialog() {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        public TimePickerFragment(){}

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String choose = hourOfDay + ":" + minute;
            Log.d(TAG, choose);

            Calendar cur_cal = new GregorianCalendar();
            cur_cal.setTimeInMillis(System.currentTimeMillis());

            Calendar cal = new GregorianCalendar();
            cal.add(Calendar.DAY_OF_YEAR, cur_cal.get(Calendar.DAY_OF_YEAR));
            cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
            cal.set(Calendar.MINUTE, minute);
            cal.set(Calendar.SECOND, cur_cal.get(Calendar.SECOND));
            cal.set(Calendar.MILLISECOND, cur_cal.get(Calendar.MILLISECOND));
            cal.set(Calendar.DATE, cur_cal.get(Calendar.DATE));
            cal.set(Calendar.MONTH, cur_cal.get(Calendar.MONTH));

            switch (time){
                case "start": {
                    ((MainActivity) getActivity()).setTimeStart(choose);
                    textStartTimeChoose.setText(choose);

                    Intent intent = new Intent(getActivity(), TimeUpReceiver.class);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 1253, intent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                    }
                    break;
                case "stop": {
                    ((MainActivity) getActivity()).setTimeStop(choose);
                    textStopTimeChoose.setText(choose);

                    Intent intent = new Intent(getActivity(), TimeStopReceiver.class);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 1254, intent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                    }
                    break;
            }

        }
    }

}
