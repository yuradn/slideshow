package com.example.yura.slideshow;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.yura.slideshow.fragment.PictureFragment;
import com.example.yura.slideshow.fragment.SettingsFragment;
import net.rdrei.android.dirchooser.DirectoryChooserActivity;

public class MainActivity extends ActionBarActivity {

    public static final String ACTION_STOP_MY_APPLICATION = "com.example.yura.slideshow.timestop";
    public static final String MAIN_PREFERECES = "MyPref";
    public static final String STATE_AUTO_EXECUTE = "autoexec";
    public static final String STATE_START_POWER_ON = "power";
    public static final String STATE_DIRECTORY = "dir";
    public static final String STATE_INTERVAL = "intervalS";
    public static final String PICTURE_FRAGMENT = PictureFragment.class.getSimpleName();

    public static final int REQUEST_CHOOSER_DIR  = 6761;
    public static final int MENU_STOP_PLAY       = 29000;

    private String directory="";
    private int interval=5;
    private String timeStart="";
    private String timeStop="";

    private boolean ifChargeStartActivity = false;
    private boolean autoexec = false;

    private boolean isPlay        = false;
    private boolean changeDuration= false;
    private boolean changeDir     = false;

    private SharedPreferences sPref;

    private final static String TAG = MainActivity.class.getSimpleName();
    private PictureFragment pictureFragment = null;
    private SettingsFragment settingsFragment;
    private Menu menu;
    FragmentTransaction fTrans;

    private CloseListener closeListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadDataFromSharedPreferences();

        closeListener = new CloseListener();

        Log.d(TAG, "Create Fragment");
        pictureFragment = new PictureFragment();
        settingsFragment = new SettingsFragment();
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.frgmCont, pictureFragment);
        fTrans.commit();

        Intent timeIntent = getIntent();
        if (timeIntent!=null) {
            if (timeIntent.getAction()!=null) {
                switch (timeIntent.getAction()) {
                    case "timeStop":
                        Log.d(TAG,"TimeIntent: "+timeIntent.getAction());
                        pictureFragment.stopPlay();
                        break;
                }

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.d(TAG,"Menu id= "+id);

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Log.d(TAG,"onOptionsItemSelected");
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.addToBackStack(PICTURE_FRAGMENT);
            fTrans.add(R.id.frgmCont, settingsFragment);
            //android.support.v7.app.ActionBar actionBar = getSupportActionBar();
            menu.findItem(R.id.action_settings).setVisible(false);
            //actionBar.hide();
            fTrans.commit();

            return true;
        }

        if (id==MENU_STOP_PLAY) {
            Log.d(TAG,"Stop play");
            pictureFragment.stopPlay();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void saveDataInSharedPreferences() {
        sPref = getSharedPreferences(MAIN_PREFERECES, MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();

        ed.putBoolean(STATE_AUTO_EXECUTE, autoexec);
        ed.putBoolean(STATE_START_POWER_ON, ifChargeStartActivity);
        ed.putString(STATE_DIRECTORY, directory);
        ed.putInt(STATE_INTERVAL, interval);

        ed.apply();
    }

    void loadDataFromSharedPreferences() {
        sPref = getSharedPreferences(MAIN_PREFERECES, MODE_PRIVATE);

        autoexec = sPref.getBoolean(STATE_AUTO_EXECUTE,false);
        ifChargeStartActivity = sPref.getBoolean(STATE_START_POWER_ON, false);
        directory = sPref.getString(STATE_DIRECTORY, "");
        interval = sPref.getInt(STATE_INTERVAL,1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MenuItem menuItem = menu.findItem(R.id.action_settings);
        if (!menuItem.isVisible()) {
            menuItem.setVisible(true);
            menu.add(1,MENU_STOP_PLAY,1,"Stop");
            if (!(directory.equals(""))) {
                pictureFragment.setKeyPlayActive();
            }
        }
        if (isPlay()) {
            Log.d(TAG,"isPlay ***************");
            if (changeDuration&&!changeDir) {
                pictureFragment.changeDuration();
                changeDuration=false;
            }
            if (changeDir) {
                pictureFragment.startPlay();
                changeDir=false;
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG,"onActivityResult)");//: requestCode= "+requestCode+" resultCode="+resultCode);
        switch (requestCode) {
            case 137833:
                if (resultCode == DirectoryChooserActivity.RESULT_CODE_DIR_SELECTED) {
                    directory = data
                            .getStringExtra(DirectoryChooserActivity.RESULT_SELECTED_DIR);
                    Log.d(TAG, directory);
                    settingsFragment.setTextChooseDirectory();
                    if (isPlay) {
                        changeDir = true;
                    }
                } else {
                    // Nothing selected
                    Log.d(TAG, "Nothing select");
                }

                break;

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        saveDataInSharedPreferences();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"onStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(ACTION_STOP_MY_APPLICATION);
        registerReceiver(closeListener,intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(closeListener);
    }

    public class CloseListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG,"Stop MyActivity");
            MainActivity.this.finish();
        }
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeStop() {
        return timeStop;
    }

    public void setTimeStop(String timeStop) {
        this.timeStop = timeStop;
    }

    public boolean isPlay() {
        return isPlay;
    }

    public void setPlay(boolean isPlay) {
        this.isPlay = isPlay;
    }

    public boolean isChangeDuration() {
        return changeDuration;
    }

    public void setChangeDuration(boolean changeDuration) {
        this.changeDuration = changeDuration;
    }

    public boolean isChangeDir() {
        return changeDir;
    }

    public void setChangeDir(boolean changeDir) {
        this.changeDir = changeDir;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public boolean isIfChargeStartActivity() {
        return ifChargeStartActivity;
    }

    public void setIfChargeStartActivity(boolean ifChargeStartActivity) {
        this.ifChargeStartActivity = ifChargeStartActivity;
    }

    public boolean isAutoexec() {
        return autoexec;
    }

    public void setAutoexec(boolean autoexec) {
        this.autoexec = autoexec;
    }

}
