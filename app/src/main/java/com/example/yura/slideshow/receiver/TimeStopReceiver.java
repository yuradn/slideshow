package com.example.yura.slideshow.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.yura.slideshow.MainActivity;

public class TimeStopReceiver extends BroadcastReceiver {
    private static final String TAG=TimeStopReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive");

        Intent i = new Intent();
        i.setAction(MainActivity.ACTION_STOP_MY_APPLICATION);
        context.sendBroadcast(i);

    }
}
